package walker

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

// argv parsing flags
var NFO, TXT, JPG, PNG, BMP, IMG, DRY, Cover, Empty bool

// Whitelist the value of a filename to whitelist
var Whitelist string

func Walk(path string) {
	filepath.Walk(path, fileaction)
	fmt.Println("Done.")
}

func rmfile(path string) {
	if !DRY {
		err := os.Remove(path)

		if err != nil {
			if os.IsExist(err) {
				fmt.Println("Error: ", err)
			}
		}

		return
	}

	fmt.Println(path, "was extinguished")
}

func Image() {
	jpg, png, bmp := &JPG, &PNG, &BMP

	if IMG {
		*jpg, *png, *bmp = true, true, true
	}
}

func emptydir(path string) bool {
	files, _ := ioutil.ReadDir(path)
	if len(files) == 0 {
		return true
	}

	return false
}

func delemptydir(path string) {
	if Empty && emptydir(path) {
		rmfile(path)
	}
}

func fileaction(path string, info os.FileInfo, err error) error {

	// fmt.Println(path)

	base := filepath.Base(path)
	ext := filepath.Ext(path)
	filename := strings.TrimSuffix(base, ext)
	filename = strings.ToLower(filename)

	// Skip all the content of a folder if it contain in name "cover" or "artwork"
	parentdir := strings.ToLower(filepath.Dir(path))
	if strings.Contains(parentdir, "artwork") || strings.Contains(parentdir, "cover") {
		return filepath.SkipDir
	}

	switch {
	case filename == "cover" && Cover:
		return nil
	case filename == Whitelist:
		return nil
	case info.IsDir():
		delemptydir(path)
	case ext == ".nfo" && NFO:
		rmfile(path)
	case ext == ".jpg" && JPG:
		rmfile(path)
	case ext == ".txt" && TXT:
		rmfile(path)
	case ext == ".png" && PNG:
		rmfile(path)
	case (ext == ".bmp" || ext == ".dib") && BMP:
		rmfile(path)
	}

	return nil
}
