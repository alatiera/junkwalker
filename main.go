package main

import (
	"flag"

	"github.com/alatiera/junkwalker/src/walker"
)

func main() {
	// Rest of the passed arguments, used for debugging
	// Usage: -p Path, default path is '.'
	providedpath := flag.String("P", ".", "Provided path")

	flag.BoolVar(&walker.NFO, "nfo", false, "Include *.nfo files")
	flag.BoolVar(&walker.TXT, "txt", false, "Include *.txt files")
	flag.BoolVar(&walker.JPG, "jpg", false, "Include *.jpg files")
	flag.BoolVar(&walker.PNG, "png", false, "Include *.png files")
	flag.BoolVar(&walker.BMP, "bmp", false, "Include *.bmp and *.dib files")
	flag.BoolVar(&walker.IMG, "img", false, "Include image/picture files")
	flag.BoolVar(&walker.Cover, "cover", false, "Whitelist *cover* files")
	flag.BoolVar(&walker.Empty, "emptyoff", true, "Remove Empty Directories")
	flag.BoolVar(&walker.DRY, "dry-run", false, "Run the script without removing anything")

	flag.StringVar(&walker.Whitelist, "wl", "", "Name of a file to whitelist, Case sensitive")

	flag.Parse()
	walker.Image()

	// args := flag.Args()
	// fmt.Println(args, *providedpath, walker.nfo, walker.txt, walker.jpg, waler.png)

	walker.Walk(*providedpath)
}
