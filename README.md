# Junkwalker
A script to clean up your media folders of all the ugly .nfo, .txt files.

## Usecase:
If you happen to run a media server like emby or plex,
or use a fancy music player that fetches album covers,
or your video player tries to findout out what you watch(?) and ends up caching additional info,
you might end up like I did with an ugly collection of useless files that clutter the view
whenever I happen to use a filemanager instead of a Media client. 

## Todo:

- [x] Argv path parsing

- [x] toggle file formats with cmd args(-nfo, -jpg)

- [x] Add support for whitelisting names(ex. cover.*)

- [x] Group Filetypes (-img, inlcudes .png, .jpg, .bmp)

- [x] Delete empty dirs

- [x] Add Dry-run option

- [ ] Split the code, Clean it up


## Installation:

Assuming that $GOPATH/bin is in your $PATH

`go get github.com/alatiera/junkwalker `

`Junkwalker --help`

```
Usage of junkwalker:
  -P string
        Provided path (default ".")
  -bmp
        Include *.bmp and *.dib files
  -cover
        Whitelist *cover* files
  -dry-run
        Run the script without removing anything
  -emptyoff
        Remove Empty Directories
  -img
        Include image/picture files
  -jpg
        Include *.jpg files
  -nfo
        Include *.nfo files
  -png
        Include *.png files
  -txt
        Include *.txt files
  -wl string
        Name of a file to whitelist, Case sensitive
```

## Usage:

**Path:**

Defaults to the path its run from, but you can pass a different with -P flag.

`junkwalker -P path/to/some/folder`

`junkwalker` is equivelant to `junkwalker -P .`

**Remove crap:**

`junkwalker -img` It will remove all files with .jpg, .png, .bmp, .dib extension

`junkwalker -nfo -txt` will remove all .txt and .nfo files

**Whitelisting:**

Currently only one name is supported for whitlisting, cause my implementation is crap.

To whitelist a file it has to exactly match its filename **without** the last .ext extension

for example to whitelist foobar.jpg you would run:

`junkwalker -jpg -wl foobar`

There is also a -cover flag, By enabling it the script will ignore files with filename **exactly** "cover.*"
Targeted at cleanup of a Music folder.
The script ignores directories/folders that cointain cover or artwork in their name.

**Music Folder Cleanup Example:**

For example to cleanup a music folder of multiple coverfiles you would run:

`junkwalker -img -cover` 

or 

`junkwalker -P ~/Music/ wl epicphoto -img -cover` 

**Empty Directory/Folder Removal:**

Theres a flag to disable the auto-empty folders removal.

`junkwalker -emptyoff` 

**DRY-Run:**
Theres also a Dry run that would print what would get deleted.

`junkwalker -P ~/Music -img -cover -dry-run` 